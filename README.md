# LedWall Arduino Program #

* *A simple application to show what can be done with the LedWall*
* Version 3b1
* 6/8/2016
* Tested and working on Arduino Mega 2560 in combination with LedwallAndroidApp v4.1

## Features ##

### Snake ###
![snake](https://bitbucket.org/ledwall/lap/downloads/snake_very_small.gif)

### Minesweeper ###
![snake](https://bitbucket.org/ledwall/lap/downloads/minesweeper_very_small.gif)

* Displaying a welcome message on startup
* Displaying messages sent over Ethernet
* Displaying single pixels sent over Ethernet
* Playing Snake (In combination with LedwallAndroidApp v4.1)
* Playing Connect4 (In combination with LedwallAndroidApp v4.1)
* Playing MineSweeper(In combination with LedwallAndroidApp v4.1)

## Requirements ##
* Arduino Microcontroller (tested only on Arduino Mega 2560)
* Arduino IDE (version 1.8.1, http://www.arduino.org/previous-releases)
* Arduino Ethernet Shield
* LedWall

## Setup ##

1. Copy the libraries folder into your arduino library-folder (usually in user/documents/arduino/libraries)
2. Open the .ino-file in the arduino IDE
3. Press CTRL+U to compile and upload the program to your arduino

## Configuration ##
The Program can be configured by editing the constants at the top of the .ino-File.

* USEUDP: If this is defined, the program will listen for incoming UDP-packets, otherwise it will listen for HTTP-requests
* SHOWAFTERGAMESCREENS: If this is defined, the program will show the score reached while playing snake after you have died as well as the player's name who won Connect4
* DEBUG: Enables debug output over Serial with baudrate 9600 

## Creator ##

*[Christopher Stelzm�ller](https://twitter.com/_stoez) (c.stelzmueller@yahoo.com)*