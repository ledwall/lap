//by stoez
#include <arduino.h>
#include <ArduinoJson.h>
#include <SPI.h>
#include <Ethernet.h>
#include <TimerOne.h>
#include <Letters.h>
#include <LetterUtil.h>
#include <LEDLib.h>
#include <LEDWall.h>
#include <Message.h>
#include <Color.h>

//comment the following line to use HTTP Post requests, uncomment to use Udp
#define USEUDP
//keep in mind that HTTP Post is slower than a medium sized turtle

//uncomment following line to show score after snake game has finished, comment to just wait for the game to start again
#define SHOWAFTERGAMESCREENS

//debug options
//comment the following line to disable debug output, uncomment to enable
//#define DEBUG
//carefull: debug options slow the program down drastically

#ifdef DEBUG
#define dprint(x) Serial.print (x)
#define dprintln(x) Serial.println (x)
#define dprintlnf(x,f) Serial.println(x,f)
#else
#define dprint(x)
#define dprintln(x)
#define dprintlnf(x,f)
#endif

//some configuration for ethernet
byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress staticIP ({192, 168, 1, 141});
String myIp = "";

//Configuration for UDP/HTTP requests
#ifdef USEUDP
#include <EthernetUdp.h>
unsigned int udpPort = 8888;
const int UDP_PACKET_MAX_SIZE = 500;
char packetBuffer[UDP_PACKET_MAX_SIZE];
EthernetUDP Udp;
#else
unsigned int httpPort = 80;
EthernetClient client;
EthernetServer server = EthernetServer(httpPort);
#endif

//keys for deserializing json structures
const String ROWKEY = "row";
const String COLUMNKEY = "col";
const String COLOURKEY = "colour";

//configure ledwall pins on the arduino
uint8_t dataPins[] = {2, 11};
uint8_t clockPins[] = {3, 12};
const int lights = 50;
const int chains = 2;
LEDWall lw = LEDWall(lights, dataPins, clockPins, chains);

//variables for storing and switching between displaying modes
enum Mode {
  SinglePixels,
  Msg,
  SnakeScore,
  AfterConnectFour,
  MineSweeper
};
static Mode currentMode = Msg;
boolean modeChanged = true;

//setup for SinglePixels mode
boolean pixelsChanged = false;
const unsigned char PIXELWIDTH = 14;
const unsigned char PIXELHEIGHT = 7;
static uint16_t pixels[PIXELHEIGHT][PIXELWIDTH];

//setup for message, this message will be shown after starting the arduino
static char* message = "Willkommen an der HTL Leonding!";
const long DELAYTIME = 200;
uint16_t fgCol = Color::red;
uint16_t bgCol = Color::black;

#ifdef SHOWAFTERGAMESCREENS
//setup for timing the snake score
unsigned long prevMillis = 0;
const long TIME_AFTER_SNAKE_FINISH = 2500;
const long TIME_AFTER_CONNECT4_FINISH = 2000;
int score = -1;
boolean changeModeToScore = false;
int lastCol = -1;
#endif

//configure colors
uint16_t colors[6] = {
  Color::black,
  Color::blue,
  Color::green,
  Color::red,
  Color::white,
  Color::rosy
};
const unsigned char SNAKE_ALIVE_COLOR_ID = 1;
const unsigned char SNAKE_DEAD_COLOR_ID = 3;

/*
 *  Function:   setupEthernet
 *  -------------------------
 *  This function is used to setup Ethernet using DHCP.
 *  If USEUDP is defined, this method starts listening for UDP packets on an
 *  above specified port. Otherwise the method will start listening for HTTP-
 *  POST requests on another port.
 *
 *  returns: a boolean which is true if it was able to retrieve an IP-Address over
 *  DHCP and false when it wasn't.
 */

 boolean setupEthernet() {
  if (Ethernet.begin(mac) == 0) {
    //return false if there is no dhcp server in the network
    Ethernet.begin(mac, staticIP);
    Serial.println("now using static ip address");
  }
  //if ip could be retrieved, store it in a String to be able to display it later
  for (byte thisByte = 0; thisByte < 4; thisByte++) {
    int i = Ethernet.localIP()[thisByte];
    myIp += i;
    if (thisByte < 3) {
      myIp += ".";
    }
  }
  //Write the IP-Address and protocol the program is using to the Serial-Port
  Serial.println("My Ip is " + myIp);

  #ifdef USEUDP
  Udp.begin(udpPort);
  Serial.print("Listening for Udp Packets on ");
  Serial.println(udpPort, DEC);
  #else
  server.begin();
  Serial.print("HTTP Server running on ");
  Serial.println(httpPort, DEC);
  #endif

  return true;
}

/*
 *  Function:   sendResponse
 *  -------------------------
 *  Sends the given response to a connected client, indicating to the sender of the
 *  message that we have received it.
 *
 *  httpClient: client to which the response will be sent
 *  msg:        Message that will be the body part of the response
 *
 *  NOTE: this function will only be compiled if USEUDP is not defined
 */

 #ifndef USEUDP
 void sendResponse(EthernetClient httpClient, String msg) {
  httpClient.println("HTTP/1.1 200 OK");
  httpClient.println("Content-Type: application/json");
  httpClient.println("Connection: close");
  httpClient.println();

  httpClient.print("{'res':'");
  httpClient.print(msg);
  httpClient.println("'}");
  dprintln("sent response");
}
#endif

/*
 *  Function:   getHttpInput
 *  -------------------------
 *  This function checks if there is a client which wants to connect to the arduino over HTTP.
 *  If there is a connection to a client read and analyze the HTTP-headers.
 *  If there is a Content-Length header present it calls the getContent-Method to retrieve said content.
 *  If you received a content, it will be analyzed using the executeJson-method.
 *  Afterwards the function sends a response to the client and closes the connection.
 *
 *  NOTE: this function will only be compiled if USEUDP is not defined
 */

 #ifndef USEUDP
 void getHttpInput() {
  EthernetClient httpClient = server.available();
  if (httpClient) {
    String input = "";
    int contentLength = 0;
    dprintln("new Client");
    String currentLine = "";
    boolean currentLineIsBlank = true;
    while (httpClient.connected()) {
      if (httpClient.available()) {
        char c = httpClient.read();
        input += c;
        currentLine += c;

        if (c == '\n' && currentLineIsBlank) {
          break;
        }

        if (c == '\n') {
          currentLineIsBlank = true;
          //check if the request has any content in its body
          int contentPos = 0;
          if (contentPos = currentLine.indexOf("Content-Length: ") > -1) {
            for (int i = contentPos; i < currentLine.length(); i++) {
              char in = currentLine.charAt(i) - '0';
              if (in >= 0 && in <= 9) {
                contentLength *= 10;
                contentLength += in;
              }
            }
          }
          currentLine = "";
        }

        else if (c != '\r') {
          currentLineIsBlank = false;
        }
      }
    }
    dprintln(input);
    dprintln(contentLength);
    //if the request has a body read it
    if (contentLength) {
      String content = getContent(httpClient, contentLength);
      dprintln(content);
      sendResponse(httpClient, "Ok");

      //analyze and execute the
      executeJson(content);
    }
    //if there is no content just tell the client that you received an empty request
    else {
      sendResponse(httpClient, "No Content...");
    }
    dprintln("*****************************");

    //give the client some time to retrieve the response before closing the connection
    delay(1);
    httpClient.stop();
  }
}
#endif

/*
 *  Function:   getContent
 *  -------------------------
 *  Retrieves content of given length from a connected HTTP-connection represented by the httpClient-variable
 *
 *  httpClient:     client the request comes from
 *  contentLength:  Length of the message in the body of the HTTP-request
 *
 *  returns:        the content of the request as a string
 *
 *  NOTE: this function will only be compiled if USEUDP is not defined
 */

 #ifndef USEUDP
 String getContent(EthernetClient httpClient, int contentLength) {
  String content = "";
  for (int i = 0; i < contentLength && httpClient.connected() && httpClient.available(); i++) {
    char c = httpClient.read();
    content += c;
  }
  return content;
}
#endif


/*
 *  Function:   getUdpInput
 *  -------------------------
 *  Checks if there is a packet incoming via UDP-connection and if it is, that package is stored in the packetBuffer.
 *  Maximal size of UDP-packets is stored in the UDP_PACKET_MAX_SIZE constant which is defined at the start of the program.
 *
 *  The retrieved message will be analyzed in the executeJson method
 *
 *  NOTE: this function will only be compiled if USEUDP is defined
 */

 #ifdef USEUDP
 void getUdpInput() {
  if (Udp.parsePacket()) {
    dprint("Received Udp packet from ");
    #ifdef DEBUG
    IPAddress remote = Udp.remoteIP();
    for (byte thisByte = 0; thisByte < 4; thisByte++) {
      Serial.print(remote[thisByte], DEC);
      if (thisByte < 3) {
        Serial.print(".");
      }
    }
    Serial.print(", port ");
    Serial.println(Udp.remotePort());
    #endif
    //clear the packetBuffer and store the retrieved packet in it.
    memset(packetBuffer, 0, sizeof(packetBuffer));
    Udp.read(packetBuffer, UDP_PACKET_MAX_SIZE);
    dprint("with content: ");
    dprintln(packetBuffer);

    //analyze the retrieved packet
    executeJson(packetBuffer);
  }
}
#endif


/*
 *  Function:   setSnakeAliveToDeadColor
 *  -------------------------
 *  Change the color of the snake which is stored somewhere in the pixels-Array. The snake's color will be changed
 *  from the color stored in the SNAKE_ALIVE_COLOR_ID index in the color-Array to the color in SNAKE_DEAD_COLOR_ID.
 *
 *  If SHOWAFTERGAMESCREENS is defined, this function will also count how long the snake is and compute the score from that value
 */

 void setSnakeAliveToDeadColor() {
  #ifdef SHOWAFTERGAMESCREENS
  score = -3;
  prevMillis = millis();
  currentMode = SnakeScore;
  modeChanged = true;
  #endif

  pixelsChanged = true;
  for (int i = 0; i < PIXELHEIGHT; i++) {
    for (int j = 0; j < PIXELWIDTH; j++) {
      if (pixels[i][j] == colors[SNAKE_ALIVE_COLOR_ID]) {
        pixels[i][j] = colors[SNAKE_DEAD_COLOR_ID];
        #ifdef SHOWAFTERGAMESCREENS
        score++;
        #endif
      }
    }
  }
  #ifdef SHOWAFTERGAMESCREENS
  dprintlnf(score, DEC);
  #endif
}

/*
 *  Function:   executeJson
 *  -------------------------
 *  This function is used to analyze and execute a json string. It uses the ArduinoJson Library to decode the json string.
 *
 *  json: the string that will be analyzed
 */

 void executeJson(String json) {
  StaticJsonBuffer<500> jsonBuffer;

  //try to parse the string to Json-Array
  JsonArray& array = jsonBuffer.parseArray(json);
  if (!array.success()) {
    dprintln("Json string is not an array");

    //if the string can't be parsed to an array try to parse it into a JsonObject
    JsonObject& obj = jsonBuffer.parseObject(json);
    if (obj.success()) {
      dprintln("Json string is an object");

      //check if the object tries to change the LedWall's mode
      if (obj.containsKey("mode")) {
        currentMode = (Mode)obj["mode"].as<int>();
        modeChanged = true;
        dprintln("Changed mode");
        return;
      }

      //check if the object tries to set a new Message or change the message's color
      else if (obj.containsKey("msg") || obj.containsKey("fgcol")) {
        if (obj.containsKey("msg")) {
          message = " ";
          String msg = obj["msg"];
          for (int i = 0; i < msg.length(); i++) {
            message[i] = msg.charAt(i);
          }
          message[msg.length()] = ' ';
          message[msg.length() + 1] = '\0';
          dprintln("Changed message");
        }
        //colors can be passed either as a String, like "red" or as a JsonObject containing a RGB-Code
        if (obj.containsKey("fgcol")) {
          uint16_t c = Color::white;

          if (obj["fgcol"].as<String>() != NULL) {
            String fgCol = obj["fgcol"];
            dprintln("Got color " + fgCol + " from object");
            if (fgCol == "red")
            c = Color::red;
            else if (fgCol == "green")
            c = Color::green;
            else if (fgCol == "blue")
            c = Color::blue;
          }

          byte r = obj["fgcol"]["r"];
          byte g = obj["fgcol"]["g"];
          byte b = obj["fgcol"]["b"];
          if (r | g | b != 0) {
            c = Color::calcColor(r, g, b);
          }
          fgCol = c;
          dprintln("changed message colour");
        }
        modeChanged = true;
      }

      //check if the object tries to send a special signal
      else if (obj.containsKey("sig")) {
        int sig = obj["sig"];
        //special signal 0 sets the snakes state from alive to dead
        if (sig == 0) {
          setSnakeAliveToDeadColor();
          lastCol = -1;
          dprintln("Set snake color to dead");
        }
        #ifdef SHOWAFTERGAMESCREENS
        else if (sig == 1) {
          prevMillis = millis();
          score = -1;
          currentMode = AfterConnectFour;
          modeChanged = true;
          dprintln("received connect4 winner signal");
        }
        #endif
      }
    }
    else {
      dprintln("Json string is not an object");
    }
    return;
  }

  //if the string is a json array, iterator through its objects and parse them to blocks
  for (int i = 0; i < array.size(); i++) {
    int row = array[i][ROWKEY];
    int col = array[i][COLUMNKEY];
    int colour = array[i][COLOURKEY];
    //check if the block contained valid information and if so set the specified leds
    if (row != 0 && col != 0 && colour != 0) {
      setLed(row - 1, col - 1, colour - 1);
    }
    else {
      dprintln("Couldn't parse json string, wrong object keys");
    }
  }
}

/*
 *  Function:   setLed
 *  -------------------------
 *  This function changes the colour of the specified led in the pixels array and sets the pixelsChanged-flag to
 *  indicate that there are changes in the pixels-Array that need to be written to the ledwall.
 *
 *  col:      column of the led in the pixels-Array
 *  row:      row of the led in the pixels-Array
 *  colour:   colour-Index the led will be set to
 */

 void setLed(int row, int col, int colour) {
  dprint("col: "); dprintln(col);
  dprint("row: "); dprintln(row);
  dprint("colour: "); dprintln(colour);

  #ifdef SHOWAFTERGAMESCREENS
  lastCol = colour;
  #endif
  if (currentMode != MineSweeper) {
    pixels[row][col] = colors[colour];
  }
  else {
    /*  mode is minesweeper, so different colors will be used
         value   meaning
         0 - 8:  amount of mines nearby
         9:      the block is flagged
         10:     the block is question-marked
         11:     block shows a mine
         12:     not yet used, not opened block
         http://www.colorpicker.com/color-chart/

        fields with more than 5 mines nearby are VERY unlikely
        */
        dprint("minesweeper color: "); dprintln(colour);
        switch (colour) {
      case 0: 
      pixels[row][col] = Color::calcColor(0,0,20);
      break;
      case 1:   //dark blue
      pixels[row][col] = Color::blue;
      break;
      case 2:   //rain blue
      pixels[row][col] = Color::calcColor(0,100,0);
      break;
      case 3:   //dirty blue
      pixels[row][col] = Color::calcColor(70,40,0);
      break;
      case 4:   //lavender violet
      pixels[row][col] = Color::calcColor(85,26,139);
      break;
      case 5:   //reisy blue
      pixels[row][col] = Color::calcColor(139,28,98);
      break;
      case 6:  //needs some improvements...
      pixels[row][col] = Color::calcColor(238,173,14);
      break;
      case 7: 
      pixels[row][col] = Color::calcColor(47,79,79);
      break;
      case 8: 
      pixels[row][col] = Color::calcColor(71,71,71);
      break;
      case 9: 
      pixels[row][col] = Color::calcColor(255,255,0);
      break;
      case 10: 
      pixels[row][col] = Color::white;
      break;
      case 11:  
      pixels[row][col] = Color::red;
      break;
      default:
      pixels[row][col] = Color::black;
      break;
    }
  }
  pixelsChanged = true;
}

/*
 *  Function:   setup
 *  -------------------------
 *  This function is called once upon starting the arduino and contains some setup for using all components of the program.
 *
 */

 void setup() {
  //initialize Serial port with baud rate 9600
  Serial.begin(9600);

  //initialize and clear ledwall
  lw.begin();
  lw.clear(1);

  //clear the pixels-Array
  memset(pixels, Color::black, sizeof(pixels));

  //try to setup Ethernet using DHCP
  if (!setupEthernet()) {
    Serial.println("Failed to configure Ethernet using dhcp");
  }
}

/*
 *  Function:   loop
 *  -------------------------
 *  This function is called continuously during the execution of the sketch and checks if there is new input from an ethernet-
 *  connection (either udp or http, depending on the defined constants), if so starts a routine to get, analyze and execute that
 *  input.
 *  Afterwards, this function displays some kind of information on the ledwall.
 *  What kind of information is displayed on the ledwall varies according to the value of currentMode.
 *  if the mode was recently changed, some configuration for the new mode will be done in this method as well.
 */

 void loop() {
  #ifdef USEUDP
  getUdpInput();
  #else
  getHttpInput();
  #endif

  if (modeChanged) {
    //setup the message mode
    if (currentMode == Msg) {
      Message::setMsg(message);
      dprint("Set Message to: ");
      dprintln(message);
      Message::setColors(fgCol, bgCol);
      modeChanged = false;
    }
    else if (currentMode == SinglePixels || currentMode == MineSweeper) {
      memset(pixels, Color::black, sizeof(pixels));
      pixelsChanged = true;
      modeChanged = false;
    }
    //The following lines of code will only be compiled if SHOWAFTERGAMESCREENS has been defined
    #ifdef SHOWAFTERGAMESCREENS
    else if (currentMode == SnakeScore) {
      //this mode is kind of tricky, because it will only be shown after a certain time has passed after
      //the snake's color has been set to dead. That time is stored in TIME_AFTER_SNAKE_FINISH
      if (millis() - prevMillis > TIME_AFTER_SNAKE_FINISH) {
        itoa(score, message, 10);
        Message::setMsg(message);
        dprint("Set Message to: ");
        dprintln(message);
        Message::setColors(fgCol, bgCol);

        //compute how far the message has to scroll left to be in the middle of the ledwall
        int slideToLeft = 0;
        for (int i = 0; i < strlen(message); i++) {
          int cols, rows;
          Letters::getLetter(message[i], &cols, &rows);
          slideToLeft += cols + 1;
        }
        slideToLeft -= 1;

        slideToLeft = (PIXELWIDTH + slideToLeft) / 2;

        for (int i = 0; i < slideToLeft; i++) {
          Message::scroll();
        }
        Message::draw(lw);
        modeChanged = false;
      }
      else {
        //if the timespan has not yet passed after a snake-game has been finished, the snake should still be displayed
        currentMode = SinglePixels;
        changeModeToScore = true;
        delay(50);
      }
    }
    else if (currentMode == AfterConnectFour ) {
      //TODO: check if last pixel of connect4 has already been displayed...
      //otherwise display that one...
      //maybe fix by adding a wait before sending the connect4 win signal...
      if (millis() - prevMillis > TIME_AFTER_CONNECT4_FINISH) {
        message = "P";
        message[1] = '0' + lastCol;
        message[2] = '\0';
        Message::setMsg(message);
        dprint("Set Message to: ");
        dprintln(message);
        Message::setColors(colors[lastCol], colors[0]);//compute how far the message has to scroll left to be in the middle of the ledwall
        int slideToLeft = 0;
        for (int i = 0; i < strlen(message); i++) {
          int cols, rows;
          Letters::getLetter(message[i], &cols, &rows);
          slideToLeft += cols + 1;
        }
        slideToLeft -= 2;

        slideToLeft = (PIXELWIDTH + slideToLeft) / 2;

        for (int i = 0; i < slideToLeft; i++) {
          Message::scroll();
        }

        Message::draw(lw);
        modeChanged = false;
      }
      else {
        //if the timespan has not yet passed after a Connect4-game has been finished, the snake should still be displayed
        currentMode = SinglePixels;
        changeModeToScore = true;
        delay(50);
      }
    }
    #endif
  }

  if (currentMode == Msg) {
    Message::scroll();
    Message::draw(lw);
    delay(DELAYTIME);
  }
  else if (currentMode == SinglePixels || currentMode == MineSweeper) {
    if (pixelsChanged) {
      //clear the ledwall and write the values in the pixels-Array out
      lw.clear(0, Color::black);
      for (int row = 0; row < PIXELHEIGHT; row++) {
        for (int col = 0; col < PIXELWIDTH; col++) {
          lw.setPixelAt(row, col, pixels[row][col]);
        }
      }
      lw.show();
      pixelsChanged = false;
    }

    #ifdef SHOWAFTERGAMESCREENS
    //make the program check again if enough time has passed to show snake's score
    if (changeModeToScore) {
      changeModeToScore = false;
      if (score == -1){
        currentMode = AfterConnectFour;

      }
      else{
        currentMode = SnakeScore;
      }
      modeChanged = true;
    }
    #endif
  }
}

