#include "LetterUtil.h"

char LetterUtil::getData(int x, int y, int cols, int rows, char* data)
{
	if (x >= rows || y >= cols || x < 0 || y < 0)
		return -1;
	return data[cols*x+y];
}