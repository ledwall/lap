
#ifndef letterutil_h
#define letterutil_h

class LetterUtil
{
	private:
		LetterUtil() {} //nicht Instanzierbar
		LetterUtil(const LetterUtil&) {} //nicht kopierbar
		LetterUtil& operator=(const LetterUtil&) {} //nicht zuweisbar
		~LetterUtil() {}
	public:
		static char getData(int x, int y, int cols, int rows, char* data);
};


#endif