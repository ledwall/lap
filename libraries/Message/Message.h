#include <arduino.h>
#include <Color.h>
#include <LEDWall.h>

#ifndef message_h
#define message_h

class Message
{
	private:
		Message() {} //nicht Instanzierbar
		Message(const Message&) {} //nicht kopierbar
		Message& operator=(const Message&) {} //nicht zuweisbar
		~Message() {}
		static void loadNextLetter();
		
	public:
		static void printPuffer();
		static void scroll();
		static void draw(LEDWall wall);
		static void setMsg(char* msg);
		static void setForeground(uint16_t color);
		static void setBackground(uint16_t color);
		static void setColors(uint16_t foreground, uint16_t background);
};

#endif