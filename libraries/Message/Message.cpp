#include "Message.h"
#include <arduino.h>
#include <Color.h>
#include <Letters.h>
#include <LEDWall.h>
#include <LetterUtil.h>

static uint16_t foreground;
static uint16_t background;
static char message[106];
static int pufferCols[5];
static int pufferRows[5];
static int firstPufferPos = -1; //erstes zeichen kann au�erhalb des bildschirms sein
static int msgIndex = 0; //Position in der Nachricht
static int columnPos = 14; //column position
static int msgLen = 0;

static char puffer[5][100];

void Message::loadNextLetter()
{
	if (msgIndex == msgLen)
	{
		msgIndex = 0;
	}
	int rows,cols;
	strcpy(puffer[4], Letters::getLetter(message[msgIndex], &cols, &rows));
	pufferCols[4] = cols;
	pufferRows[4] = rows;
	msgIndex++;
}

void Message::scroll()
{
	if (columnPos > 0)
		columnPos--;
	if (columnPos == 0)
		firstPufferPos++;
	if (firstPufferPos == pufferCols[0]) //gibt es noch spalten f�r den ersten buchstaben?
	{ //buchstabe nicht mehr sichtbar - n�chsten buchstaben laden
		firstPufferPos = 0;
		for(int i = 0; i < 4; i++)
		{
			strcpy(puffer[i], puffer[i+1]);
			pufferCols[i] = pufferCols[i+1];
			pufferRows[i] = pufferRows[i+1];
		}
		loadNextLetter();
	}
}

void Message::printPuffer()
{
	for(int i = 0; i < 5; i++)
	{
		for(int k = 0; k < pufferRows[i]; k++)
		{
			for(int j = 0; j < pufferCols[i]; j++)
			{
				Serial.print(LetterUtil::getData(k,j, pufferCols[i], pufferRows[i], puffer[i]));
			}
			Serial.println();
			Serial.println("------------------------------");
		}
	}
}

void Message::draw(LEDWall wall)
{
	wall.clear(0, background);
	int allCols = columnPos;
	for (int i = 0; i < 5; i++) //alle zeichen
	{
		int k = (i == 0) ? firstPufferPos : 0;
		for(; k < pufferCols[i]; k++) //anzahl columns zum jwl. buchstaben
		{
			if (allCols == 14)
				break;
			for(int l = 0; l < pufferRows[i]; l++)
			{
				int c = LetterUtil::getData(l, k, pufferCols[i], pufferRows[i], puffer[i]);
				char k = 'A';
				if (c > -1)
				{
					k = c;
				}
				wall.setPixelAt(l, allCols, (k == 'X' ? foreground : background));
			}
			allCols++;
		}
	}
	wall.show();
}

void Message::setMsg(char* msg)
{
	/*Serial.println(msg);
	Serial.println("--------------------");*/
	if (strlen(msg) > 100)
	{
		return;
	}
	for (int i = 0; i < 106; i++)
	{
		message[i] = 0;
	}
	for (int i = 0; i < 5; i++)
	{
		for (int k = 0; k < 100; k++)
		{
			puffer[i][k] = 0;
		}
	}
	strcpy(message, msg);
	strcat(message, "  ");
	msgIndex = 0;
	int i;
	msgLen = strlen(msg);
	for(i = 0; i < 5 && i < msgLen; i++) //5 Zeichen laden
	{
		int rows,cols;
		strcpy(puffer[i], Letters::getLetter(message[msgIndex], &cols, &rows));
		pufferCols[i] = cols;
		pufferRows[i] = rows;
		msgIndex++;
	}
	for(; i < 5; i++)
	{
		pufferCols[i] = 0;
		pufferRows[i] = 0;
	}
	columnPos = 14;
	firstPufferPos = 0;
}

void Message::setForeground(uint16_t color)
{
	foreground = color;
}
void Message::setBackground(uint16_t color)
{
	background = color;
}
void Message::setColors(uint16_t fg, uint16_t bg)
{
	setForeground(fg);
	setBackground(bg);
}