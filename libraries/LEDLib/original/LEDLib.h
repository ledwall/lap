#ifndef LEDLIB
#define LEDLIB
#if ARDUINO >= 100
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif

class LEDLib {
 private:
  uint8_t cpumax;

 public:
  LEDLib();
  LEDLib(uint16_t lightCount, uint8_t* dataPins, uint8_t* clockPins, uint8_t chainCount);
  void begin();
  void show();
  //void doSwapBuffersAsap(uint16_t idx);
  void setPixelColor(uint16_t n, uint8_t r, uint8_t g, uint8_t b);
  void setPixelColor(uint16_t n, uint16_t c);
  void setCPUmax(uint8_t m);
  void clearLEDs(uint8_t show);
  void clearLEDs(uint8_t show, uint16_t color);
  void updatePerArray(uint16_t colors[], uint8_t show);
  uint16_t numPixels(void);
  int getChainCount(void);
  int getLedsPerChain(void);
};
#endif