#include <TimerOne.h>
#include "LEDLib.h"

enum Sendmode {
  START, //clear vars
  HEADER, //send header
  DATA, //send led data
  DONE //just clock out the leds
};

static uint16_t *pixels; //pixels[LEDID] = Color of the LED

static uint16_t clear = 0x8000;

//Variables used in interrupt routine
static Sendmode sendMode;  //What will we send?
static uint16_t ledIndex; //Indicates which LED will be sent
static byte blankCt; //Used in interrupt
static byte BitCount;
static byte lastdata = 0; 
static uint16_t swapAsap = 0; //flag to indicate that the colors need an update asap

//Init variables
static uint8_t *dataPins;
static uint8_t *clockPins;
static uint8_t chainCount;
static int ledAmount; //Total amount of LED's
static uint16_t ledsPerChain;


//returns the needed pin index
int getRealPos(int actLedIndex)
{
	int ct = 0;
	int lights = 0;
	while (ct < chainCount)
	{
		if (actLedIndex >= lights && actLedIndex < lights+ledsPerChain)
			return ct;
		ct++;
		lights += ledsPerChain;
	}
	return -1;
}

/*
	Interrupt routine.
	Frequency is set in setup().
	Called once for every bit of data sent
	In your code, set global sendMode to 0 to re-send the data to the pixels
	Otherwise it will just send clocks.
*/
void LedOut() 
{
	int idx = getRealPos(ledIndex);
	switch(sendMode)
	{
		case DONE:
		{
		    /*
			If you want to swap a LED right now you could manage this by doing it like this:
			if (swapAsap>0)
			{
				if(!blankCt)
				{
					BitCount=0;
					ledIndex = swapAsap;
					sendMode = HEADER;
					swapAsap = 0;
				}
			}
			*/
			break;
		}
		case DATA:
		{
			if ((1 << (15-BitCount)) & pixels[ledIndex])
			{
				if (!lastdata)
				{
					digitalWrite(dataPins[idx], 1);
					lastdata = 1;
				}
			}
			else
			{
				if (lastdata)
				{
					digitalWrite(dataPins[idx], 0);
					lastdata = 0;
				}
			}
			BitCount++;
			if (BitCount == 16) //Last bit for this LED?
			{
				ledIndex++; //Move to next LED
				if (ledIndex < ledAmount)  //Need to send more LEDS ?
					BitCount = 0; //Reset the count 
				else
				{
					digitalWrite(dataPins[idx], 0);
					lastdata = 0;
					sendMode = DONE;
				}
			}
			break;
		}
		case HEADER:
		{
			if (BitCount < 32)
			{
				digitalWrite(dataPins[idx], 0);
				lastdata = 0;
				BitCount++;
				if (BitCount == 32)
				{
					sendMode = DATA;
					ledIndex = 0;
					BitCount = 0;
				}
			}
			break;
		}
		case START:
		{
			if (!blankCt) //As soon as current pwm is done
			{
				BitCount = 0;
				ledIndex = 0;
				sendMode = HEADER;
			}
			break;
		}
	}
	//Clock LED's - must be done for every chain we have!
	for (int i = 0; i < chainCount; i++)
	{
		digitalWrite(clockPins[i], HIGH);
		digitalWrite(clockPins[i], LOW);
	}
	blankCt++;
}

/*Empty Constructor for the arduino compiler*/
LEDLib::LEDLib()
{}
/*
Constructor

lightCount: number of lights per chain
_dataPins[]: dataPins of the chains 
_clockPins[]: clockpins of the chains
_chainCount: number of chains that are in use
*/
LEDLib::LEDLib(uint16_t lightCount, uint8_t* _dataPins, uint8_t* _clockPins, uint8_t _chainCount)
{
	dataPins = _dataPins;
	clockPins = _clockPins;
	ledsPerChain = lightCount;
	ledAmount = lightCount*_chainCount; //LED Amount = lights per chain * chainCount
	chainCount = _chainCount;
	cpumax = 50;
	sendMode = DONE;
	BitCount = ledIndex = blankCt = 0;
	pixels = (uint16_t *)malloc(ledAmount);
}

//Initialize the timer, the pins and attach the interrupt routine
void LEDLib::begin()
{
	Serial.println("Set Pinmode");
	Serial.flush();
	for (int i = 0; i < chainCount; i++)
	{
		pinMode(dataPins[i], OUTPUT);
		pinMode(clockPins[i], OUTPUT);
	}
	Serial.println("Set cpu max");
	Serial.flush();
	setCPUmax(cpumax);
	Serial.println("Attach");
	Serial.flush();
	Timer1.attachInterrupt(LedOut);  // attaches callback() as a timer overflow interrupt
}


//Refreshes the LEDs on the chains
void LEDLib::show()
{
	sendMode = START;
}

/*If a LED should be updated immediately

void LEDLib::doSwapBuffersAsap(uint16_t idx)
{
	  swapAsap = idx;
}
*/

//Set the color of a LED
void LEDLib::setPixelColor(uint16_t n, uint8_t r, uint8_t g, uint8_t b)
{
	if (n >= ledAmount) 
		return;
	uint32_t c = r;
	c <<= 5;
	c |= g;
	c <<= 5;
	c |= b;       //           | red| grn |blue
	c |= 0x8000; //32768 dec = 1000000000000000 bin
	pixels[n] = c;
}

//Clears all LED's
//show: != 0 -> update wall
void LEDLib::clearLEDs(uint8_t show)
{
	for (int i = 0; i < ledAmount; i++)
		pixels[i] = clear; 
	if (show != 0)
		sendMode = START;
}

void LEDLib::clearLEDs(uint8_t show, uint16_t color)
{
	for(int i = 0; i < ledAmount; i++)
		pixels[i] = color;
	if (show != 0)
		sendMode = START;
}

//Set the color of a LED
//n: LEDid - between 0 and ledAmount
//c: the 15 bit color 
void LEDLib::setPixelColor(uint16_t n, uint16_t c)
{
	if (n >= ledAmount) return;

	pixels[n] = 0x8000 | c;
}


//The ticks between the interrupt are set.
//m: cpu_max
void LEDLib::setCPUmax(uint8_t m)
{
	  cpumax = m;

	  // each clock out takes 20 microseconds max
	  long time = 100;
	  time *= 25;   // 20 microseconds per
	  time /= m;    // how long between timers
	  Timer1.initialize(55);
}

//Updates the LEDs with an array - LED 0 = colors[0]
//colors[]: The colors which the LEDs should be set
//show: Send to Chains?
void LEDLib::updatePerArray(uint16_t colors[], uint8_t show)
{
	uint16_t arrSize = sizeof(colors) / sizeof(colors[0]);
	if (ledAmount >= arrSize)
		return;
	
	for (int i = 0; i < arrSize; i ++)
	{
		pixels[i] = colors[i] | 0x8000;
	}
	if (show != 0)
		sendMode = START;
}

//Returns the Amount of LEDs
uint16_t LEDLib::numPixels(void)
{
	return ledAmount;
}

//Getter for chainCount
int getChainCount(void)
{
	return chainCount;
}

//Getter for ledsPerChain
int getLedsPerChain(void)
{
	return ledsPerChain;
}