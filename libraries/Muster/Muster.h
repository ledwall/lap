#pragma once

//#include <cstdint>//will probably not work on arduino (only for visual studio intellisense)
#include <arduino.h>

#include "Color.h"
#include "LEDWall.h"

class Muster {
public:
    Muster(LEDWall *_wall);

    void showSmilieHappy(uint16_t color);
    void showSmilieSad(uint16_t color);

    void showRandomColorBlinking(); // CMD_SHOW_RANDOM_BLINKING
    
    void initColorrow(); //TODO: Rename Method
    void initGradient(int _hueDiff = 10);
    
    //TODO: Rename Methods
    void showSomethingI(uint16_t color); // CMD_SHOW_FILL
    void showSomethingII(); // CMD_SHOW_RANDOM_COLORROW
    void showSomethingIII(uint16_t color); //WTF rosy
    void showSomethingIV(); // CMD_SHOW_RANDOM_COLORROW2 
    void showSomethingV(int huediff = 60); // CMD_SHOW_GRADIENT_CIRCLE
    void showSomethingVI(); // CMD_SHOW_GRADIENT
    void showSomethingVII(int huediff = 10); // CMD_SHOW_V
    void showSomethingVIII(int startpos, uint16_t color);
    void showSomethingIX(int huediff = 10); // CMD_SHOW_GRADIENT_DIAGONAL
    
private:
    LEDWall *wall;

    static const int xCount = 14;
    static const int yCount = 7;

    int cl;
    uint16_t colorrow[14];

    int currHue;
    int hueDiff;
    
    
    void showSmilieWithoutMouth(uint16_t color);
    
    void innerRing(uint16_t color);
    void firstRing(uint16_t color);
    void secondRing(uint16_t color);
    void thirdRing(uint16_t color);
    void fourthRing(uint16_t color);
    void fifthRing(uint16_t color);
    void sixthRing(uint16_t color);
    void seventhRing(uint16_t color);
    void eigthRing(uint16_t color);
};

