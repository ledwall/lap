#include "Muster.h"


Muster::Muster(LEDWall *_wall) {
    wall = _wall;
}

void Muster::showSmilieHappy(uint16_t color) {
    showSmilieWithoutMouth(color);

    wall->setPixelAt(5, 4, color); //Mouth
    wall->setPixelAt(6, 5, color);
    wall->setPixelAt(6, 6, color);
    wall->setPixelAt(6, 7, color);
    wall->setPixelAt(6, 8, color);
    wall->setPixelAt(5, 9, color);
}

void Muster::showSmilieSad(uint16_t color) {
    showSmilieWithoutMouth(color);

    wall->setPixelAt(6, 4, color); //Mouth
    wall->setPixelAt(5, 5, color);
    wall->setPixelAt(5, 6, color);
    wall->setPixelAt(5, 7, color);
    wall->setPixelAt(5, 8, color);
    wall->setPixelAt(6, 9, color);
}

void Muster::showSmilieWithoutMouth(uint16_t color) {
    wall->setPixelAt(1, 4, color); //Eye One
    wall->setPixelAt(2, 4, color);
    wall->setPixelAt(1, 5, color);
    wall->setPixelAt(2, 5, color);

    wall->setPixelAt(1, 8, color); //Eye Two
    wall->setPixelAt(2, 8, color);
    wall->setPixelAt(1, 9, color);
    wall->setPixelAt(2, 9, color);

    wall->setPixelAt(3, 6, color); //Nose
    wall->setPixelAt(3, 7, color);
}

void Muster::showRandomColorBlinking() {
    uint16_t theEpiColor = Color::calcColor(random(0, 255), random(0, 255), random(0, 255));

    for (int i = 0; i < 14; i++)
        for (int j = 0; j < 7; j++)
            wall->setPixelAt(j, i, theEpiColor);
}


void Muster::initColorrow() {
    for (int i = 0; i < 14; i++)
        colorrow[i] = Color::black;
}

void Muster::initGradient(int _hueDiff) {
    currHue = 0;
    hueDiff = _hueDiff;
}


void Muster::showSomethingI(uint16_t color) {
    if (cl > 98)
        cl = 0;

    int lcl = cl / 14;
    for (int i = 0; i < lcl; i++)
        for (int j = 0; j < 14; j++)
            wall->setPixelAt(i, j, color);

    for (int i = 0; i < cl % 14; i++)
        wall->setPixelAt(lcl, i, color);


    cl++;
}

void Muster::showSomethingII() {
    for (int i = 13; i > 0; i--)
        colorrow[i] = colorrow[i - 1];

    colorrow[0] = Color::calcColor(random(0, 255), random(0, 255), random(0, 255));

    for (int i = 0; i < 14; i++)
        for (int j = 0; j < 7; j++)
            wall->setPixelAt(j, i, colorrow[i]);
}

void Muster::showSomethingIII(uint16_t color) {
    for (int i = 0; i < 14; i++)
        colorrow[i] = random(0, 7);

    for (int i = 0; i < 14; i++)
        for (int j = 6; j >= colorrow[i]; j--)
            wall->setPixelAt(j, i, Color::rosy);
}

void Muster::showSomethingIV() {
    uint16_t theRandomColor = Color::calcColor(random(0, 255), random(0, 255), random(0, 255));
    for (int i = 0; i < 14; i++)
        for (int j = 0; j < 7; j++)
            if (i % 2 == 0) {
                wall->setPixelAt(j, i, Color::green);
            } else {
                wall->setPixelAt(j, i, theRandomColor);
            }
}

void Muster::showSomethingV(int huediff) {
    double s = 1;
    double v = 1;
    int colorspace = 360;

    currHue += hueDiff;
    if (currHue > 360)
        currHue %= 360;
    int i = 0;

    uint16_t col = Color::calcColor(&Color::calcColorFromHSV((currHue + hueDiff * (i + 8)) % colorspace, s, v));
    innerRing(col);
    col = Color::calcColor(&Color::calcColorFromHSV((currHue + hueDiff * (i + 7)) % colorspace, s, v));
    firstRing(col);
    col = Color::calcColor(&Color::calcColorFromHSV((currHue + hueDiff * (i + 6)) % colorspace, s, v));
    secondRing(col);
    col = Color::calcColor(&Color::calcColorFromHSV((currHue + hueDiff * (i + 5)) % colorspace, s, v));
    thirdRing(col);
    col = Color::calcColor(&Color::calcColorFromHSV((currHue + hueDiff * (i + 4)) % colorspace, s, v));
    fourthRing(col);
    col = Color::calcColor(&Color::calcColorFromHSV((currHue + hueDiff * (i + 3)) % colorspace, s, v));
    fifthRing(col);
    col = Color::calcColor(&Color::calcColorFromHSV((currHue + hueDiff * (i + 2)) % colorspace, s, v));
    sixthRing(col);
    col = Color::calcColor(&Color::calcColorFromHSV((currHue + hueDiff * (i + 1)) % colorspace, s, v));
    seventhRing(col);
    col = Color::calcColor(&Color::calcColorFromHSV((currHue + hueDiff * (i)) % colorspace, s, v));
    eigthRing(col);
}

void Muster::showSomethingVI() {
    currHue += hueDiff;
    if (currHue > 360)
        currHue %= 360;

    for (int i = 0; i < xCount; i++) {
        uint16_t col = Color::calcColor(&Color::calcColorFromHSV((currHue + hueDiff * i) % 360, 0.9, 1));

        for (int j = 0; j < yCount; j++)
            wall->setPixelAt(j, i, col);
    }
}

void Muster::showSomethingVII(int huediff) {
    double s = 1;
    double v = 1;
    int colorspace = 360;
    int j = 0;
    currHue += hueDiff;
    if (currHue > 360)
        currHue %= 360;
    int i = 0;
    uint16_t col = Color::calcColor(&Color::calcColorFromHSV((currHue + hueDiff * j) % colorspace, s, v));
    for (j = 0; j < 7; j++) {

        wall->setPixelAt(j, j, col);
        wall->setPixelAt(j, 13 - j, col);
        col = Color::calcColor(&Color::calcColorFromHSV((currHue + hueDiff * j) % colorspace, s, v));

    }
    /* for (j = 0; j < 7; j++) {

       wall->setPixelAt(j, j, col);
       wall->setPixelAt(j, 13-j, col);
       col = Color::calcColor(&Color::calcColorFromHSV((currHue + hueDiff * j) % colorspace, s, v));

   }*/

}

void Muster::showSomethingVIII(int startpos, uint16_t color) {
    for (int j = 0; j < 7; j++) {
        int y = startpos + j - 6;
        if (y >= 0 && y < 14)
            wall->setPixelAt(j, y, color);
    }
}

void Muster::showSomethingIX(int huediff) {
    double s = 1;
    double v = 1;
    int colorspace = 360;

    currHue += hueDiff;
    if (currHue > 360)
        currHue %= 360;

    for (int i = 0; i < 20; i++) {
        uint16_t col = Color::calcColor(&Color::calcColorFromHSV((currHue + hueDiff * i) % colorspace, s, v));
        showSomethingVIII(i, col);

    }
}


void Muster::innerRing(uint16_t color) {
    wall->setPixelAt(2, 6, color);
    wall->setPixelAt(3, 6, color);
    wall->setPixelAt(4, 6, color);
    wall->setPixelAt(2, 7, color);
    wall->setPixelAt(3, 7, color);
    wall->setPixelAt(4, 7, color);
}

void Muster::firstRing(uint16_t color) {
    wall->setPixelAt(1, 6, color);
    wall->setPixelAt(1, 7, color);
    wall->setPixelAt(5, 6, color);
    wall->setPixelAt(5, 7, color);
    wall->setPixelAt(2, 8, color);
    wall->setPixelAt(3, 8, color);
    wall->setPixelAt(4, 8, color);
    wall->setPixelAt(2, 5, color);
    wall->setPixelAt(3, 5, color);
    wall->setPixelAt(4, 5, color);
}

void Muster::secondRing(uint16_t color) {
    wall->setPixelAt(0, 6, color);
    wall->setPixelAt(0, 7, color);
    wall->setPixelAt(6, 6, color);
    wall->setPixelAt(6, 7, color);
    wall->setPixelAt(5, 5, color);
    wall->setPixelAt(5, 8, color);
    wall->setPixelAt(1, 5, color);
    wall->setPixelAt(1, 8, color);

    wall->setPixelAt(2, 9, color);
    wall->setPixelAt(3, 9, color);
    wall->setPixelAt(4, 9, color);

    wall->setPixelAt(2, 4, color);
    wall->setPixelAt(3, 4, color);
    wall->setPixelAt(4, 4, color);
}

void Muster::thirdRing(uint16_t color) {

    wall->setPixelAt(2, 10, color);
    wall->setPixelAt(3, 10, color);
    wall->setPixelAt(4, 10, color);

    wall->setPixelAt(2, 3, color);
    wall->setPixelAt(3, 3, color);
    wall->setPixelAt(4, 3, color);

    wall->setPixelAt(6, 5, color);
    wall->setPixelAt(6, 8, color);
    wall->setPixelAt(0, 5, color);
    wall->setPixelAt(0, 8, color);

    wall->setPixelAt(5, 4, color);
    wall->setPixelAt(5, 9, color);
    wall->setPixelAt(1, 4, color);
    wall->setPixelAt(1, 9, color);
}

void Muster::fourthRing(uint16_t color) {
    wall->setPixelAt(2, 11, color);
    wall->setPixelAt(3, 11, color);
    wall->setPixelAt(4, 11, color);

    wall->setPixelAt(2, 2, color);
    wall->setPixelAt(3, 2, color);
    wall->setPixelAt(4, 2, color);

    wall->setPixelAt(5, 10, color);
    wall->setPixelAt(6, 9, color);
    wall->setPixelAt(1, 10, color);
    wall->setPixelAt(0, 9, color);

    wall->setPixelAt(6, 4, color);
    wall->setPixelAt(5, 3, color);
    wall->setPixelAt(0, 4, color);
    wall->setPixelAt(1, 3, color);
}

void Muster::fifthRing(uint16_t color) {
    wall->setPixelAt(2, 12, color);
    wall->setPixelAt(3, 12, color);
    wall->setPixelAt(4, 12, color);

    wall->setPixelAt(2, 1, color);
    wall->setPixelAt(3, 1, color);
    wall->setPixelAt(4, 1, color);

    wall->setPixelAt(5, 11, color);
    wall->setPixelAt(6, 10, color);
    wall->setPixelAt(1, 11, color);
    wall->setPixelAt(0, 10, color);

    wall->setPixelAt(6, 3, color);
    wall->setPixelAt(5, 2, color);
    wall->setPixelAt(0, 3, color);
    wall->setPixelAt(1, 2, color);
}

void Muster::sixthRing(uint16_t color) {
    wall->setPixelAt(2, 13, color);
    wall->setPixelAt(3, 13, color);
    wall->setPixelAt(4, 13, color);

    wall->setPixelAt(2, 0, color);
    wall->setPixelAt(3, 0, color);
    wall->setPixelAt(4, 0, color);

    wall->setPixelAt(5, 12, color);
    wall->setPixelAt(6, 11, color);
    wall->setPixelAt(1, 12, color);
    wall->setPixelAt(0, 11, color);

    wall->setPixelAt(6, 2, color);
    wall->setPixelAt(5, 1, color);
    wall->setPixelAt(0, 2, color);
    wall->setPixelAt(1, 1, color);
}

void Muster::seventhRing(uint16_t color) {
    wall->setPixelAt(5, 13, color);
    wall->setPixelAt(6, 12, color);
    wall->setPixelAt(1, 13, color);
    wall->setPixelAt(0, 12, color);

    wall->setPixelAt(6, 1, color);
    wall->setPixelAt(5, 0, color);
    wall->setPixelAt(0, 1, color);
    wall->setPixelAt(1, 0, color);
}

void Muster::eigthRing(uint16_t color) {
    wall->setPixelAt(0, 13, color);
    wall->setPixelAt(6, 13, color);
    wall->setPixelAt(0, 0, color);
    wall->setPixelAt(6, 0, color);
}