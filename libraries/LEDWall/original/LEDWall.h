#ifndef ledwall_h
#define ledwall_h
#include "LEDLib.h"
class LEDWall
{
	private:
		LEDLib chains;
		
	public:
	    
		LEDWall(uint16_t lightCount, uint8_t* _dataPins, uint8_t* _clockPins, uint8_t _chainCount);
		void setPixel(uint16_t n, uint16_t color);
		void setPixelAt(int x, int y, uint16_t color);
		void clear(uint8_t show);
		void clear(uint8_t show, uint16_t color);
		void begin(void);
		uint16_t pixelCount(void);
		void show(void);
		void drawBox(uint32_t font);
		uint8_t transformIndex(int x, int y);
};
#endif