
/*
This class extends the LEDLib class, which fit only for the actual LEDWall (2012)
If a method is not described in this class, you may look at LEDLib.cpp for a documentation
Also, look for a documentation at the SVN-Server
*/
#include <arduino.h>
#include <LEDWall.h>
#include <LEDLib.h>
#include <Letters.h>

LEDWall::LEDWall(uint16_t lightCount, uint8_t* _dataPins, uint8_t* _clockPins, uint8_t _chainCount)
{
	chains = LEDLib(lightCount, _dataPins,_clockPins, _chainCount);
}

void LEDWall::begin()
{
	Serial.println("init chains");
	Serial.flush();
	chains.begin();
}

void LEDWall::clear(uint8_t show)
{
	chains.clearLEDs(show);
}

void LEDWall::clear(uint8_t show, uint16_t color)
{
	chains.clearLEDs(show, color);
}

void LEDWall::drawBox(uint32_t font)
{
	for (int i = 0; i < 14; i++)
	{
		for (int k = 0; k < 7; k++)
		{
			if(i == 0 ||i == 13 ||k == 0 ||k == 6)
				setPixelAt(k,i, font);
		}
	}
}

void LEDWall::setPixel(uint16_t id, uint16_t color)
{
	chains.setPixelColor(id, color);
}

uint8_t GetIndexForSingleChain(int x, int y)
{
	int incrementValue = 1;
	int count = 0;
	uint8_t index = 0;
	for (int i = 0; i < 7; i++)
	{
		while (count >= 0 && count < 7)
		{
			if (count == y && i == x)
				return index;
			count += incrementValue;
			index++;
		}
		if (count == 7)
			incrementValue = -1;
		if (count == -1)
			incrementValue = 1;
		count += incrementValue;
	}
	return 0;
}
/*
Transforms a 2D index into a 1D index (= LED_ID)

*/
uint8_t LEDWall::transformIndex(int x, int y)
{
	if (y >= 7)
		return GetIndexForSingleChain(y - 7, x) + 50;
	else
		return GetIndexForSingleChain(y,x);
}

/*
Sets a pixel at a 2D position
*/
void LEDWall::setPixelAt(int x, int y, uint16_t color)
{
	setPixel(transformIndex(x,y), color);
}

void LEDWall::show()
{
	chains.show();
}