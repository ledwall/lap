#include <arduino.h>
#ifndef color_h
#define color_h

struct rgb {
    byte r;
    byte g;
    byte b;
};

class Color {
private:
    Color();

public:
    static uint16_t calcColor(byte r, byte g, byte b);
    static uint16_t calcColor(rgb *color);

    static rgb calcColorFromHSV(float h, float s, float v);
    static rgb getRGB(uint16_t color);
    
    static uint16_t red;
    static uint16_t green;
    static uint16_t blue;
    static uint16_t white;
    static uint16_t black;
    static uint16_t yellow;
    static uint16_t rosy;
};

#endif