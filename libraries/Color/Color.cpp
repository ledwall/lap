#include "color.h"
#include <arduino.h>

#define RED_MASK_555 0x7C00
#define GREEN_MASK_555 0x3E0
#define BLUE_MASK_555 0x1F

uint16_t Color::calcColor(byte r, byte g, byte b) {
	uint32_t c = 0;
	c = (r >> 3) << 5;
	c |= g >> 3;
	c <<= 5;
	c |= b >> 3;
	c |= 0x8000;
	uint16_t f = (uint16_t)c;
	return f;
}

uint16_t Color::calcColor(rgb *color) {
	return calcColor(color->r, color->g, color->b);
}

rgb Color::calcColorFromHSV(float h, float s, float v) {
	int i;
	rgb res;
	float f, p, q, t, r1, g1, b1;

	if( s == 0 ) {
		res.r = res.g = res.b = v;
		return res;
	}

	h /= 60;
	i = (int)h;
	f = h - i;
	p = v * (1 - s);
	q = v * (1 - s * f);
	t = v * (1 - s * (1 - f));

	switch(i) {
	case 0: r1 = v; g1 = t; b1 = p; break;
	case 1: r1 = q; g1 = v; b1 = p; break;
	case 2: r1 = p; g1 = v; b1 = t; break;
	case 3: r1 = p;	g1 = q;	b1 = v;	break;
	case 4:	r1 = t;	g1 = p;	b1 = v;	break;
	default: r1 = v; g1 = p; b1 = q; break;
	}

	res.r = r1 * 255;
	res.g = g1 * 255;
	res.b = b1 * 255;

	return res;
}
rgb Color::getRGB(uint16_t color) {
	rgb res;

	res.r = (color & RED_MASK_555) >> 7;
	res.g = (color & GREEN_MASK_555) >> 2;
	res.b = (color & BLUE_MASK_555) << 3;

	return res;
}

uint16_t Color::red = calcColor(255, 0, 0);
uint16_t Color::green = calcColor(0, 255, 0);
uint16_t Color::blue = calcColor(0, 0, 255);
uint16_t Color::black = calcColor(0, 0, 0);
uint16_t Color::white = calcColor(255, 255, 255);
uint16_t Color::rosy = calcColor(255, 193, 193);
uint16_t Color::yellow = calcColor(255, 255, 0);