#ifndef __INC_HALed_H
#define __INC_HALed_H

typedef byte LEDINDEX;
typedef byte COORDINATE;
typedef unsigned int COLOR;
#define NOLEDIDX -1

class HALed 
{ 
public:

	/////////////////////////////////////////////////////////////////////////////
 
	void Init(LEDINDEX numleds, COLOR display[]);
  
	COLOR* _display;  
	LEDINDEX _leds;

	COORDINATE gridY;
	COORDINATE gridX;

	static const COLOR NoColor;
	static const COLOR OffColor;
   
	/////////////////////////////////////////////////////////////////////////////

	void Show();  

	void SetRange(LEDINDEX startLED, LEDINDEX endLED, COLOR color );
	void SetAll(COLOR color );
	LEDINDEX Count(COLOR col);

	static COLOR Wheel(byte WheelPos);
	static COLOR Color(byte r, byte g, byte b);
	static void ColorToRGB(COLOR col, byte& r, byte& g, byte& b);

	void SetPixel(LEDINDEX LED, COLOR color);
	void SetPixel(COORDINATE x, COORDINATE y, COLOR color);
	
	// x/y system

	LEDINDEX Translate(COORDINATE x, COORDINATE y);
	LEDINDEX Translate(COORDINATE x, COORDINATE y,COORDINATE x0, COORDINATE y0,COORDINATE x1, COORDINATE y1);
	
	void Box(COORDINATE x0, COORDINATE y0, COORDINATE x1, COORDINATE y1, COLOR color);
	void Line(COORDINATE x0,  COORDINATE y0, COORDINATE x1, COORDINATE y1, COLOR color);
	void Fill(COORDINATE x0, COORDINATE y0, COORDINATE x1, COORDINATE y1, COLOR color);

	void Scroll(COORDINATE x0, COORDINATE y0, COORDINATE x1, COORDINATE y1, char dx, char dy, COLOR fillcol=NoColor); 

	void ScrollLeft (COORDINATE x0, COORDINATE y0, COORDINATE x1, COORDINATE y1, COLOR fillcol=NoColor);
	void ScrollRight(COORDINATE x0, COORDINATE y0, COORDINATE x1, COORDINATE y1, COLOR fillcol=NoColor);
	void ScrollDown (COORDINATE x0, COORDINATE y0, COORDINATE x1, COORDINATE y1, COLOR fillcol=NoColor);
	void ScrollUp   (COORDINATE x0, COORDINATE y0, COORDINATE x1, COORDINATE y1, COLOR fillcol=NoColor);
 
	void Scroll(char dx, char dy, COLOR fillcol=NoColor) { Scroll(0,0,gridX-1,gridY-1, dx, dy, fillcol); }; 
	void ScrollLeft (COLOR fillcol=NoColor)		{ ScrollLeft (0,0,gridX-1,gridY-1, fillcol); };
	void ScrollRight(COLOR fillcol=NoColor)		{ ScrollRight(0,0,gridX-1,gridY-1, fillcol); };
	void ScrollDown (COLOR fillcol=NoColor)		{ ScrollDown (0,0,gridX-1,gridY-1, fillcol); };
	void ScrollUp   (COLOR fillcol=NoColor)		{ ScrollUp   (0,0,gridX-1,gridY-1, fillcol); };

	void FadeOut(COORDINATE x0, COORDINATE y0, COORDINATE x1, COORDINATE y1, unsigned int delaytime); 

	void ScrollRange(byte d, COORDINATE x0, COORDINATE y0, COORDINATE x1, COORDINATE y1, char dx, char dy, unsigned int delaytime, unsigned int col=NoColor); 

	void ScrollRangeLeft (byte d, unsigned int delaytime, unsigned int col=NoColor)		{ ScrollRange(d,0,0,gridX-1,gridY-1, -1,0,delaytime, col); };
	void ScrollRangeRight(byte d, unsigned int delaytime, unsigned int col=NoColor)		{ ScrollRange(d,0,0,gridX-1,gridY-1,  1,0,delaytime, col); };
	void ScrollRangeUp   (byte d, unsigned int delaytime, unsigned int col=NoColor)		{ ScrollRange(d,0,0,gridX-1,gridY-1, 0, 1,delaytime, col); };
	void ScrollRangeDown (byte d, unsigned int delaytime, unsigned int col=NoColor)		{ ScrollRange(d,0,0,gridX-1,gridY-1, 0,-1,delaytime, col); };
	
	/// font
	
	static const byte* ToAsciiArray(char ch);
	static const byte* ToAsciiArray(char ch, const byte*& outend, byte& charXsize, char& shift);

	byte ScrollInRight(char ch, unsigned int delay, COLOR col, COLOR fillcol=OffColor); 
	byte PrintChar(char ch, COORDINATE x, COORDINATE y, COLOR col, COLOR fillcol=OffColor);
	byte PrintChar(char ch, COORDINATE x0, COORDINATE y0,COORDINATE x1, COORDINATE y1, COLOR col, COLOR fillcol=OffColor);	//center in area

private:

	static const byte _0[];
	static const byte _1[];
	static const byte _2[];
	static const byte _3[];
	static const byte _4[];
	static const byte _5[];
	static const byte _6[];
	static const byte _7[];
	static const byte _8[];
	static const byte _9[];

	static const byte _Blank[];
	static const byte _Dot[];
	static const byte _Comma[];
	static const byte _Minus[];
	static const byte _Plus[];
	static const byte _Mult[];
	static const byte _Div[];
	static const byte _Exp[];
	static const byte _A[];
	static const byte _B[];
	static const byte _C[];
	static const byte _D[];
	static const byte _E[];
	static const byte _F[];
	static const byte _G[];
	static const byte _H[];
	static const byte _I[];
	static const byte _J[];
	static const byte _K[];
	static const byte _L[];
	static const byte _M[];
	static const byte _N[];
	static const byte _O[];
	static const byte _P[];
	static const byte _Q[];
	static const byte _R[];
	static const byte _S[];
	static const byte _T[];
	static const byte _U[];
	static const byte _V[];
	static const byte _W[];
	static const byte _X[];
	static const byte _Y[];
	static const byte _Z[];
	static const byte _a[];
	static const byte _b[];
	static const byte _c[];
	static const byte _d[];
	static const byte _e[];
	static const byte _f[];
	static const byte _g[];
	static const byte _h[];
	static const byte _i[];
	static const byte _j[];
	static const byte _k[];
	static const byte _l[];
	static const byte _m[];
	static const byte _n[];
	static const byte _o[];
	static const byte _p[];
	static const byte _q[];
	static const byte _r[];
	static const byte _s[];
	static const byte _t[];
	static const byte _u[];
	static const byte _v[];
	static const byte _w[];
	static const byte _x[];
	static const byte _y[];
	static const byte _z[];
	
	static const byte* _ABC[];
	static const byte* _abc[];
	static const byte* _0123[];
	
};

extern HALed BL;

#endif

