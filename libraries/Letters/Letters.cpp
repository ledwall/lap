
#include "Letters.h"
#include <arduino.h>
static char* _A = " XXX  X   X X   X XXXXX X   X X   X ";
static char* _B = "XXXX  X   X XXXX  X   X X   X XXXX  ";
static char* _C = " XXX  X   X X     X     X   X  XXX  ";
static char* _D = "XXX   X  X  X   X X   X X  X  XXX   ";
static char* _E = "XXXXX X     XXXX  X     X     XXXXX ";
static char* _F = "XXXXX X     XXXX  X     X     X     ";
static char* _G = " XXX  X   X X     X XXX X   X  XXX  ";
static char* _H = "X   X X   X XXXXX X   X X   X X   X ";
static char* _I = " XXX   X    X    X    X   XXX ";
static char* _J = " XXXX    X     X  X  X  X  X   XX   ";
static char* _K = "X  X  X X   XX    X X   X  X  X   X ";
static char* _L = "X     X     X     X     X     XXXXX ";
static char* _M = "X   X XX XX X X X X X X X   X X   X ";
static char* _N = "XX  X XX  X X X X X X X X  XX X  XX ";
static char* _O = " XXX  X   X X   X X   X X   X  XXX  ";
static char* _P = "XXX   X  X  XXX   X     X     X     ";
static char* _Q = " XXX  X   X X   X X X X X  X   XX X ";
static char* _R = "XXXX  X   X XXXX  X  X  X   X X   X ";
static char* _S = " XXXX X      XXX      X     X XXXX  ";
static char* _T = "XXXXX   X     X     X     X     X   ";
static char* _U = "X   X X   X X   X X   X X   X  XXX  ";
static char* _V = "X   X X   X X   X X   X  X X    X   ";
static char* _W = "X   X X   X X X X X X X XX XX X   X ";
static char* _X = "X   X  X X    X     X    X X  X   X ";
static char* _Y = "X   X X   X  X X    X     X     X   ";
static char* _Z = "XXXXX    X    X    X    X     XXXXX ";
static char* _Ae = "X   X  XXX  X   X XXXXX X   X X   X ";
static char* _Oe = "X   X  XXX  X   X X   X X   X  XXX  ";
static char* _Ue = "X   X       X   X X   X X   X  XXX  ";

static char* _a = "          XXXX  XXX X  X XXXX ";
static char* _b = "X    X    XXX  X  X X  X XXX  ";
static char* _c = "           XXX X    X     XXX ";
static char* _d = "   X    X  XXX X  X X  X  XXX ";
static char* _e = "           XX  XXXX X     XXX ";
static char* _f = "  X   X X  X   XXX   X    X   ";
static char* _g = "       XXXX X   X X   X  XXXX     X  XXX  ";
static char* _h = "X    X    XXX  X  X X  X X  X ";
static char* _i = "  X   X X X ";
static char* _j = "          X       X   X XX  ";
static char* _k = "X    X    X X  XX   X X  X  X ";
static char* _l = " X    X    X    X    X     XX ";
static char* _m = "            XXXX  X X X X X X X X X ";
static char* _n = "          XXX  X  X X  X X  X ";
static char* _o = "           XX  X  X X  X  XX  ";
static char* _p = "      XXXX  X   X X   X XXXX  X     X     ";
static char* _q = "       XXX  X   X X   X  XXXX     X     X ";
static char* _r = "          X XX XX   X    X    ";
static char* _s = "           XXX XXX     X XXX  ";
static char* _t = " X  XXX  X   X   X   X  ";
static char* _u = "          X  X X  X X  X  XXX ";
static char* _v = "          X  X X  X X  X  XX  ";
static char* _w = "            X   X X X X X X X  X X  ";
static char* _y = "            X   X  X X    X     X   XX    ";
static char* _x = 		"            X X  X  X X ";
static char* _z = 		"          XXXX   X   X   XXXX ";
static char* _ae = 		"X  X      XXXX  XXX X  X XXXX ";
static char* _oe = 		"X  X       XX  X  X X  X  XX  ";
static char* _ue = 		"X  X      X  X X  X X  X  XXX ";

static char* _one = 		"  X  XX X X   X   X   X ";
static char* _two = 		" XX  X  X   X   X   X    XXXX ";
static char* _three = 	"XXX     X  XX     X    X XXX  ";
static char* _four = 	"X    X    X X  XXXX   X    X  ";
static char* _five = 	"XXXX X    XXX     X    X XXX  ";
static char* _six = 		" XXX X    XXX  X  X X  X  XX  ";
static char* _seven = 	"XXXX    X   X   X    X    X   ";
static char* _eight = 	" XX  X  X  XX  X  X X  X  XX  ";
static char* _nine = 	" XX  X  X  XXX    X    X  XX  ";
static char* _zero = 	" XX  X  X X  X X  X X  X  XX  ";

static char* _question =  
"  XX "
" X  X"
"    X"
"   X "
"  X  "
"     "
"  X  ";

static char* _alert = " X  X  X  X     X ";
static char* _doubledot = "   XX XX    XX XX ";
static char* _x_o_r =  "  X    X X  X   X                   ";
static char* _is = "        XXX     XXX     ";
static char* _close = " XX      X      X     X    X   XX   ";
static char* _open = "   XX   X    X     X      X      XX ";
static char* _minus = "           XXX                ";
static char* _semi = "     XX  XX      XX XX  ";
static char* _colon = 	"       X  X X  ";
static char* _greater = 	"    X    X    X  X  X   ";
static char* _smaller = 	"      X  X  X    X    X ";
static char* _add = 		"        X     X   XXXXX   X     X   ";
static char* _brackOpen = "XXXX X    X    X    X    XXXX ";
static char* _brackClose = "XXXX    X    X    X    X XXXX ";
static char* _camberedOpen = 		"  XXX  X    X      X     X      XXX ";
static char* _camberedClose = 		"XXX      X      X    X     X  XXX   ";
static char* _raute = 	"  X X   XXXXX   X X    X X   XXXXX   X X  ";
static char* _circa = 	"           X X X X            ";
static char* __and = 		" XX   X  X   XX   X  XX X  X   XX X ";
static char* _ss = 		
"  XX "
" X  X"
" X X "
" X  X"
" X XX"
" X   ";
static char* _proc = 	"XX   X XX  X     X     X     X  XX X   XX ";
static char* _empty  = 	"      ";
static char* _dot = 		"            XX XX ";
static char* _ch = 		"X X         ";
static char* _anf = 		"X X X X                 ";
static char* __ = 		"                         XXXX ";
static char* _backSlash = "X       X       X       X       X       X ";
static char* _slash = 	"     X     X     X     X     X     X      ";
static char* _euro = 	"  XXX  X    XXXX  XXXX   X      XXX ";
static char* _dollar = 	
"   X  "
"  XXXX"
" X X  "
"  XXX "
"   X X"
" XXXX "
"   X  ";
static char* _para = 	" XXXX X      XXX   XXX      X XXXX  ";
static char* _circ = 	" X  X X  X              ";
static char* _at = 		" XXXX  X    X X XXXX X XXXX X       XXXX  ";
static char* _def = 		"XXXXX X   X X   X X   X X   X XXXXX ";
static char* _star =    "XXX  X  X X             ";
static char wallpuffer[100];

//X = Zeilen Y = Spalten also x=4 y=5 hei�t dass es 4 hoch und 5 breit ist!!

static int _sAX = 6;
static int _sAY = 6;
static int _sBX = 6;
static int _sBY = 6;
static int _sCX = 6;
static int _sCY = 6;
static int _sDX = 6;
static int _sDY = 6;
static int _sEX = 6;
static int _sEY = 6;
static int _sFX = 6;
static int _sFY = 6;
static int _sGX = 6;
static int _sGY = 6;
static int _sHX = 6;
static int _sHY = 6;
static int _sIX = 6;
static int _sIY = 5;
static int _sJX = 6;
static int _sJY = 6;
static int _sKX = 6;
static int _sKY = 6;
static int _sLX = 6;
static int _sLY = 6;
static int _sMX = 6;
static int _sMY = 6;
static int _sNX = 6;
static int _sNY = 6;
static int _sOX = 6;
static int _sOY = 6;
static int _sPX = 6;
static int _sPY = 6;
static int _sQX = 6;
static int _sQY = 6;
static int _sRX = 6;
static int _sRY = 6;
static int _sSX = 6;
static int _sSY = 6;
static int _sTX = 6;
static int _sTY = 6;
static int _sUX = 6;
static int _sUY = 6;
static int _sVX = 6;
static int _sVY = 6;
static int _sWX = 6;
static int _sWY = 6;
static int _sXX = 6;
static int _sXY = 6;
static int _sYX = 6;
static int _sYY = 6;
static int _sZX = 6;
static int _sZY = 6;
static int _sAeX = 6;
static int _sAeY = 6;
static int _sOeX = 6;
static int _sOeY = 6;
static int _sUeX = 6;
static int _sUeY = 6;
static int _saX = 6;
static int _saY = 5;
static int _sbX = 6;
static int _sbY = 5;
static int _scX = 6;
static int _scY = 5;
static int _sdX = 6;
static int _sdY = 5;
static int _seX = 6;
static int _seY = 5;
static int _sfX = 6;
static int _sfY = 5;
static int _sgX = 7;
static int _sgY = 6;
static int _shX = 6;
static int _shY = 5;
static int _siX = 6;
static int _siY = 2;
static int _sjX = 7;
static int _sjY = 4;
static int _skX = 6;
static int _skY = 5;
static int _slX = 6;
static int _slY = 5;
static int _smX = 6;
static int _smY = 6;
static int _snX = 6;
static int _snY = 5;
static int _soX = 6;
static int _soY = 5;
static int _spX = 7;
static int _spY = 6;
static int _sqX = 7;
static int _sqY = 6;
static int _srX = 6;
static int _srY = 5;
static int _ssX = 6;
static int _ssY = 5;
static int _stX = 6;
static int _stY = 4;
static int _suX = 6;
static int _suY = 5;
static int _svX = 6;
static int _svY = 5;
static int _swX = 6;
static int _swY = 6;
static int _sxX = 6;
static int _sxY = 4;
static int _syX = 7;
static int _syY = 6;
static int _szX = 6;
static int _szY = 5;
static int _saeX = 6;
static int _saeY = 5;
static int _soeX = 6;
static int _soeY = 5;
static int _sueX = 6;
static int _sueY = 5;

static int _squestionX = 7;
static int _squestionY = 5;
static int _salertX = 6;
static int _salertY = 3;
static int _sdoubledotX = 6;
static int _sdoubledotY = 3;
static int _sx_o_rX = 6;
static int _sx_o_rY = 6;
static int _sisX = 6;
static int _sisY = 4;
static int _scloseX = 6;
static int _scloseY = 6;
static int _sopenX = 6;
static int _sopenY = 6;
static int _sminusX = 6;
static int _sminusY = 5;
static int _ssemiX = 6;
static int _ssemiY = 4;
static int _scolonX = 6;
static int _scolonY = 3;
static int _sgreaterX = 6;
static int _sgreaterY = 4;
static int _ssmallerX = 6;
static int _ssmallerY = 4;


static int _soneX = 6;
static int _soneY = 4;
static int _stwoX = 6;
static int _stwoY = 5;
static int _sthreeX = 6;
static int _sthreeY = 5;
static int _sfourX = 6;
static int _sfourY = 5;
static int _sfiveX = 6;
static int _sfiveY = 5;
static int _ssixX = 6;
static int _ssixY = 5;
static int _ssevenX = 6;
static int _ssevenY = 5;
static int _seightX = 6;
static int _seightY = 5;
static int _snineX = 6;
static int _snineY = 5;
static int _szeroX = 6;
static int _szeroY = 5;

static int _saddX = 6;
static int _saddY = 6;
static int _sbrackOpenX = 6;
static int _sbrackOpenY = 5;
static int _sbrackCloseX = 6;
static int _sbrackCloseY = 5;
static int _scamberedOpenX = 6;
static int _scamberedOpenY = 6;
static int _scamberedCloseX = 6;
static int _scamberedCloseY = 6;

static int _srauteX = 6;
static int _srauteY = 7;
static int _scircaX = 6;
static int _scircaY = 5;
static int _sandX = 6;
static int _sandY = 6;
static int _sssX = 6;
static int _sssY = 5;
static int _sprocX = 6;
static int _sprocY = 7;

static int _semptyX = 6;
static int _semptyY = 1;
static int _sdotX = 6;
static int _sdotY = 3;
static int _schX = 6;
static int _schY = 2;
static int _sanfX = 6;
static int _sanfY = 4;
static int _s_X = 6;
static int _s_Y = 5;
static int _sbackSlashX = 6;
static int _sbackSlashY = 7;
static int _sslashX = 6;
static int _sslashY = 7;
static int _seuroX = 6;
static int _seuroY = 6;
static int _sdollarX = 7;
static int _sdollarY = 6;
static int _sparaX = 6;
static int _sparaY = 6;
static int _scircX = 6;
static int _scircY = 4;
static int _satX = 6;
static int _satY = 7;
static int _sdefX = 6;
static int _sdefY = 6;
static int _sstarX = 6;
static int _sstarY = 4;

char* Letters::getLetter(char letter, int* columns, int* rows)
{
	int lsd = (int)letter;
	if(lsd == 223 || lsd == -15457 || lsd == -61)
	{
			*columns = _sssY;
			*rows = _sssX;
			return _ss; 
	}
	if (lsd == -30)
	{
		*columns = _seuroY;
		*rows = _seuroX;
		return _euro;
	}
	switch(letter)
	{
		case 'A': {
			*columns = _sAY;
			*rows = _sAX;
			return _A;
			break; }
		case 'B': {
			*columns = _sBY;
			*rows = _sBX;
			return _B;
			break; }
		case 'C': {
			*columns = _sCY;
			*rows = _sCX;
			return _C;
			break; }
		case 'D': {
			*columns = _sDY;
			*rows = _sDX;
			return _D;
			break; }
		case 'E': {
			*columns = _sEY;
			*rows = _sEX;
			return _E;
			break; }
		case 'F': {
			*columns = _sFY;
			*rows = _sFX;
			return _F;
			break; }
		case 'G': {
			*columns = _sGY;
			*rows = _sGX;
			return _G;
			break; }
		case 'H': {
			*columns = _sHY;
			*rows = _sHX;
			return _H;
			break; }
		case 'I': {
			*columns = _sIY;
			*rows = _sIX;
			return _I;
			break; }
		case 'J': {
			*columns = _sJY;
			*rows = _sJX;
			return _J;
			break; }
		case 'K': {
			*columns = _sKY;
			*rows = _sKX;
			return _K;
			break; }
		case 'L': {
			*columns = _sLY;
			*rows = _sLX;
			return _L;
			break; }
		case 'M': {
			*columns = _sMY;
			*rows = _sMX;
			return _M;
			break; }
		case 'N': {	
			*columns = _sNY;
			*rows = _sNX;
			return _N;
			break; }
		case 'O': {
			*columns = _sOY;
			*rows = _sOX;
			return _O;
			break; }
		case 'P': {
			*columns = _sPY;
			*rows = _sPX;
			return _P;
			break; }
		case 'Q': {
			*columns = _sQY;
			*rows = _sQX;
			return _Q;
			break; }
		case 'R': {
			*columns = _sRY;
			*rows = _sRX;
			return _R;
			break; }
		case 'S': {
			*columns = _sSY;
			*rows = _sSX;
			return _S;
			break; }
		case 'T': {
			*columns = _sTY;
			*rows = _sTX;
			return _T;
			break; }
		case 'U': {
			*columns = _sUY;
			*rows = _sUX;
			return _U;
			break; }
		case 'V': {
			*columns = _sVY;
			*rows = _sVX;
			return _V;
			break; }
		case 'W': {
			*columns = _sWY;
			*rows = _sWX;
			return _W;
			break; }
		case 'X': {
			*columns = _sXY;
			*rows = _sXX;
			return _X;
			break; }
		case 'Y': {
			*columns = _sYY;
			*rows = _sYX;
			return _Y;
			break; }
		case 'Z': {
			*columns = _sZY;
			*rows = _sZX;
			return _Z;
			break; }
		case '�': {
			*columns = _sAeY;
			*rows = _sAeX;
			return _Ae;
			break; }
		case '�': {
			*columns = _sOeY;
			*rows = _sOeX;
			return _Oe;
			break; }
		case '�': {
			*columns = _sUeY;
			*rows = _sUeX;
			return _Ue;
			break; }
		case '?': {
			*columns = _squestionY;
			*rows = _squestionX;
			return _question;
			break; }
		case '!': {
			*columns = _salertY;
			*rows = _salertX;
			return _alert;
			break; }
		case ':': {
			*columns = _sdoubledotY;
			*rows = _sdoubledotX;
			return _doubledot;
			break; }
		case '^': {
			*columns = _sx_o_rY;
			*rows = _sx_o_rX;
			return _x_o_r;
			break; }
		case '=': {
			*columns = _sisY;
			*rows = _sisX;
			return _is;
			break; }
		case ')': {
			*columns = _scloseY;
			*rows = _scloseX;
			return _close;
			break; }
		case '(': {
			*columns = _sopenY;
			*rows = _sopenX;
			return _open;
			break; }
		case '-': {
			*columns = _sminusY;
			*rows = _sminusX;
			return _minus;
			break; }
		case ';': {
			*columns = _ssemiY;
			*rows = _ssemiX;
			return _semi;
			break; }
		case 'a': {
			*columns = _saY;
			*rows = _saX;
			return _a;
			break; }
		case 'b': {
			*columns = _sbY;
			*rows = _sbX;
			return _b;
			break; }
		case 'c': {
			*columns = _scY;
			*rows = _scX;
			return _c;
			break; }
		case 'd': {
			*columns = _sdY;
			*rows = _sdX;
			return _d;
			break; }
		case 'e': {
			*columns = _seY;
			*rows = _seX;
			return _e;
			break; }
		case 'f': {
			*columns = _sfY;
			*rows = _sfX;
			return _f;
			break; }
		case 'g': {
			*columns = _sgY;
			*rows = _sgX;
			return _g;
			break; }
		case 'h': {
			*columns = _shY;
			*rows = _shX;
			return _h;
			break; }
		case 'i': {
			*columns = _siY;
			*rows = _siX;
			return _i;
			break; }
		case 'j': {
			*columns = _sjY;
			*rows = _sjX;
			return _j;
			break; }
		case 'k': {
			*columns = _skY;
			*rows = _skX;
			return _k;
			break; }
		case 'l': {
			*columns = _slY;
			*rows = _slX;
			return _l;
			break; }
		case 'm': {
			*columns = _smY;
			*rows = _smX;
			return _m;
			break; }
		case 'n': {
			*columns = _snY;
			*rows = _snX;
			return _n;
			break; }
		case 'o': {
			*columns = _soY;
			*rows = _soX;
			return _o;
			break; }
		case 'p': {
			*columns = _spY;
			*rows = _spX;
			return _p;
			break; }
		case 'q': { 
			*columns = _sqY;
			*rows = _sqX;
			return _q;
			break; }
		case 'r': {
			*columns = _srY;
			*rows = _srX;
			return _r;
			break; }
		case 's': {
			*columns = _ssY;
			*rows = _ssX;
			return _s;
			break; }
		case 't': {
			*columns = _stY;
			*rows = _stX;
			return _t;
			break; }
		case 'u': {
			*columns = _suY;
			*rows = _suX;
			return _u;
			break; }
		case 'v': {
			*columns = _svY;
			*rows = _svX;
			return _v;
			break; }
		case 'w': {
			*columns = _swY;
			*rows = _swX;
			return _w;
			break; }
		case 'x': {
			*columns = _sxY;
			*rows = _sxX;
			return _x;
			break; }
		case 'y': {
			*columns = _syY;
			*rows = _syX;
			return _y;
			break; }
		case 'z': {
			*columns = _szY;
			*rows = _szX;
			return _z;
			break; }
		case '�': {
			*columns = _saeY;
			*rows = _saeX;
			return _ae;
			break; }
		case '�': {
			*columns = _soeY;
			*rows = _soeX;
			return _oe;
			break; }
		case '�': {
			*columns = _sueY;
			*rows = _sueX;
			return _ue;
			break; }
		case ',': {
			*columns = _scolonY;
			*rows = _scolonX;
			return _colon;
			break; }
		case '>': {
			*columns = _sgreaterY;
			*rows = _sgreaterX;
			return _greater;
			break; }
		case '<': {
			*columns = _ssmallerY;
			*rows = _ssmallerX;
			return _smaller;
			break; }
		case '1': {
			*columns = _soneY;
			*rows = _soneX;
			return _one;
			break; }
		case '2': {
			*columns = _stwoY;
			*rows = _stwoX;
			return _two;
			break; }
		case '3': {
			*columns = _sthreeY;
			*rows = _sthreeX;
			return _three;
			break; }
		case '4': {
			*columns = _sfourY;
			*rows = _sfourX;
			return _four;
			break; }
		case '5': {
			*columns = _sfiveY;
			*rows = _sfiveX;
			return _five;
			break; }
		case '6': {
			*columns = _ssixY;
			*rows = _ssixX;
			return _six;
			break; }
		case '7': {
			*columns = _ssevenY;
			*rows = _ssevenX;
			return _seven;
			break; }
		case '8': {
			*columns = _seightY;
			*rows = _seightX;
			return _eight;
			break; }
		case '9': {
			*columns = _snineY;
			*rows = _snineX;
			return _nine;
			break; }
		case '0': {
			*columns = _szeroY;
			*rows = _szeroX;
			return _zero;
			break; }
		case '+': {
			*columns = _saddY;
			*rows = _saddX;
			return _add;
			break; }
		case '[': {
			*columns = _sbrackOpenY;
			*rows = _sbrackOpenX;
			return _brackOpen;
			break; }
		case ']': {
			*columns = _sbrackCloseY;
			*rows = _sbrackCloseX;
			return _brackClose;
			break; }
		case '{': {
			*columns = _scamberedOpenY;
			*rows = _scamberedOpenX;
			return _camberedOpen;
			break; }
		case '}': {
			*columns = _scamberedCloseY;
			*rows = _scamberedCloseX;
			return _camberedClose;
			break; }
		case '#': {
			*columns = _srauteY;
			*rows = _srauteX;
			return _raute;
			break; }
		case '~': {
			*columns = _scircaY;
			*rows = _scircaX;
			return _circa;
			break; }
		case '&': {
			*columns = _sandY;
			*rows = _sandX;
			return __and;
			break; }
		case '�': {
			*columns = _sssY;
			*rows = _sssX;
			return _ss;
			break; }
		case '%': {
			*columns = _sprocY;
			*rows = _sprocX;
			return _proc;
			break; }
		case ' ': {
			*columns = _semptyY;
			*rows = _semptyX;
			return _empty;
			break; }
		case '.': {
			*columns = _sdotY;
			*rows = _sdotX;
			return _dot;
			break; }
		case '\'': {
			*columns = _schY;
			*rows = _schX;
			return _ch;
			break; }
		case '\"': {
			*columns = _sanfY;
			*rows = _sanfX;
			return _anf;
			break; }
		case '_': {
			*columns = _s_Y;
			*rows = _s_X;
			return __;
			break; }
		case '\\': {
			*columns = _sbackSlashY;
			*rows = _sbackSlashX;
			return _backSlash;
			break; }
		case '/': {
			*columns = _sslashY;
			*rows = _sslashX;
			return _slash;
			break; }
		case '�': {
			*columns = _seuroY;
			*rows = _seuroX;
			return _euro;
			break; }	
		case '$': {
			*columns = _sdollarY;
			*rows = _sdollarX;
			return _dollar;
			break; }
		case '�': {
			*columns = _sparaY;
			*rows = _sparaX;
			return _para;
			break; }
		case '�': {
			*columns = _scircY;
			*rows = _scircX;
			return _circ;
			break; }
		case '@': {
			*columns = _satY;
			*rows = _satX;
			return _at;
			break; }
		case '*': {
			*columns = _sstarY;
			*rows = _sstarX;
			return _star;
			break; }
		default: {
			*columns = _semptyY;
			*rows = _semptyX;
			return _empty;
			break; }
	}
	*columns = _semptyY;
			*rows = _semptyX;
			return _empty;
}
