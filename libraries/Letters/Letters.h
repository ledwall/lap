#include <arduino.h>
#ifndef letters_h
#define letters_h

class Letters 
{
	private:
		Letters() {} //nicht Instanzierbar
		Letters(const Letters&) {} //nicht kopierbar
		Letters& operator=(const Letters&) {} //nicht zuweisbar
		~Letters() {}
	public:
		static char* getLetter(char ch, int* columns, int* rows);
};

#endif